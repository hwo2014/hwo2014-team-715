package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.internal.LinkedTreeMap;

public class Main {
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];
        String trackName = "france";
        String password ="fVFzxiUutZ";
        int carCount = 2;

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new CreateRace(botName, botKey, trackName, password, carCount), new Join(botName, botKey));
    }

    final Gson gson = new Gson();
    private PrintWriter writer;
    ArrayList<LinkedTreeMap<Object, Object>> pieces;
    double acceleration = 1;
    double speed = 0;
    double previousPosition = 0;
    double previousPieceIndex = 1;
    double previousLength = 0;
    
    boolean hasTurbo = false;
    boolean isBigDecrement = false;
    
    public Main(final BufferedReader reader, final PrintWriter writer, final CreateRace createRace, final Join join) throws IOException {
        this.writer = writer;
        String line = null;
        
//        send(createRace);
        send(join);

        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            if (msgFromServer.msgType.equals("carPositions")) {
            	
            	
    			double offset = 1;
    			
    			
            	CarPositionParam carPositionParam = new CarPositionParam(msgFromServer.data);
            	//double angle = carPositionParam.getAngle();
            	int pieceIndex = (int) carPositionParam.getPieceIndex();
            	Piece thisPiece = new Piece(pieces.get(pieceIndex));
            	int nextPieceIndex = (int) ((pieceIndex + offset) % pieces.size()); 
                Piece nextPiece = new Piece(pieces.get(nextPieceIndex));
                
            	if (pieceIndex > previousPieceIndex)
            	{
            		speed = carPositionParam.getInPieceDistance() + (previousLength - previousPosition);
            	}
            	else
            	{
            		speed = carPositionParam.getInPieceDistance() - previousPosition;	
            	}
            	
            	previousLength = thisPiece.isCurve() ? Math.toRadians(thisPiece.getAngle()) * thisPiece.getRadius() : thisPiece.getLength();
                previousPosition = carPositionParam.getInPieceDistance();
            	previousPieceIndex = pieceIndex;
                
            	
            	double buffer = 0;
            	
            	double k = 0.19;
            	double b = 0.1;
            	double d = 0.03;
            	double BASE_ACCELERATION = b - d * speed > 0 ? b - d * speed : 0;
            	
            	double MIN_SPEED = 2;
            	double MAX_SPEED = 3;
            	
            	if (thisPiece.isCurve())
            	{
            		if (speed + acceleration >= thisPiece.getLength() - carPositionParam.getInPieceDistance() - buffer)
        			{
            			if (nextPiece.isCurve())
                		{
            				double radius = thisPiece.getRadius() < nextPiece.getRadius() ? thisPiece.getRadius() : nextPiece.getRadius();
                			
//                			THRESHOLD_SPEED = k * radius;
            				
                			// make sure this and next v_i just below corresponding v_f
//                			acceleration = speed * speed / thisPiece.getRadius() / k;
        					
                			if (speed > MIN_SPEED)
                			{
                				acceleration = BASE_ACCELERATION + k * radius / (speed * speed) > 1 ? 1 : k * thisPiece.getRadius() / (speed * speed);	
                			}
                			else
                			{
                				acceleration = 1;
                			}
                			
                			
//            				if (speed > THRESHOLD_SPEED)
//            				{
//            					acceleration = MIN_TURN_ACCELERATION;
//            				}
//            				else
//            				{
//            					acceleration = MAX_TURN_ACCELERATION;	
//            				}
            				// k - speed * speed / thisPiece.getRadius();
            				
//            				if (speed * speed /thisPiece.getRadius() < k)
//        					{
//        						acceleration = k - speed * speed / thisPiece.getRadius(); 
//        					}
//        					else
//        					{
//    							acceleration = speed * speed /thisPiece.getRadius() - k;
//        					}
                		}
                		else
                		{
//                			THRESHOLD_SPEED = k * thisPiece.getRadius();
                			
                			if (speed > MIN_SPEED)
                			{
                				acceleration = BASE_ACCELERATION + k * thisPiece.getRadius() / (speed * speed) > 1 ? 1 : k * thisPiece.getRadius() / (speed * speed);	
                			}
                			else
                			{
                				acceleration = 1;
                			}
                			
                			// just make sure this v_i just below v_f
//                			if (speed > THRESHOLD_SPEED)
//            				{
//            					acceleration = MIN_TURN_ACCELERATION;
//            				}
//            				else
//            				{
//            					acceleration = MAX_TURN_ACCELERATION;	
//            				}
                		}
            			
        			}
            		else
            		{
            			double radius = thisPiece.getRadius();
            			
            			if (speed > MIN_SPEED)
            			{
            				acceleration = BASE_ACCELERATION + k * thisPiece.getRadius() / (speed * speed) > 1 ? 1 : k * thisPiece.getRadius() / (speed * speed);	
            			}
            			else
            			{
            				acceleration = 1;
            			}
            		}
            		
            		
            	}
            	else
            	// This piece is straight line
            	{
            		if (nextPiece.isCurve())
            		{
            			double radius = nextPiece.getRadius();
//            			
            			// just before enter next piece
            			if (speed + acceleration >= thisPiece.getLength() - carPositionParam.getInPieceDistance() - buffer)
            			{
            				if (speed > MIN_SPEED)
                			{
                				acceleration = BASE_ACCELERATION + k * radius / (speed * speed) > 1 ? 1 : k * radius / (speed * speed);	
                			}
                			else
                			{
                				if (speed < MAX_SPEED)
                				{
                					acceleration = 1;
                				}
                				else
                				{
                					acceleration = 0.9 * acceleration;
                					System.out.print("ah this guy, speed = " + speed);
                				}
                				
                			}
            				
            			}
            			else
            			{
            				if (speed > MIN_SPEED)
                			{
                				acceleration = BASE_ACCELERATION + k * radius / (speed * speed) > 1 ? 1 : k * radius / (speed * speed);	
                			}
                			else
                			{
                				acceleration = 1;//acceleration;
                			}
            			}
            		}
            		else
            		{
            			// as fast as possible
            			acceleration = 1;
            			
//            			if (hasTurbo)
//            			{
//            				send(new Turbo());
//            				hasTurbo = false;
//            			}
            			
            		}
            	}
            	
//                
//                if (nextPiece.isCurve())
//                {
//                	double nextAngle = (double) nextPiece.getAngle();
//                	double nextRadius = (double) nextPiece.getRadius();
//                	
//                	
//                	boolean isBigThisAngle = angle > THRESHOLD_ANGLE || angle < -THRESHOLD_ANGLE;
//                	boolean isBigNextAngle = nextAngle > THRESHOLD_ANGLE || nextAngle < -THRESHOLD_ANGLE; 
//                	boolean isSmallNextRadius = nextRadius < 150;
//                	
//                	if (isBigNextAngle && isSmallNextRadius)
//                	{
//                		
//                		
//                		if (acceleration >= MIN_ACCELERATION)
//                		{
//                			
//                			
//                			double decrement = isBigThisAngle ? BIG_DECREMENT : SMALL_DECREMENT;
//                			isBigDecrement = isBigThisAngle;
//                			
//                			acceleration = ((acceleration - decrement > MIN_ACCELERATION) ? acceleration - decrement : MIN_ACCELERATION);
//                			acceleration = acceleration > TURNING_CAP ? TURNING_CAP : acceleration;
//                		}
//                		else
//                		{
//                			acceleration = MIN_ACCELERATION;
//                		}
//                	}
//                	else
//                	{
//                		acceleration = 1;
//                	}
//                }
//                else
//                {
//                	acceleration = 1;
//                }
                
                
                
                send(new Throttle(acceleration));
                
                //System.out.print("piece: " + pieceIndex + "speed: " + speed + ", acceleration: " + acceleration + "\n");
                System.out.print("SPEED: " + speed + "\n");
//                System.out.print("THRESHOLD_SPEED: " + THRESHOLD_SPEED + "\n");
//                if (pieceIndex == 15)
//                {
//                	System.out.print("speed at 15: " + speed + "\n");	
//                }
            }
            else if (msgFromServer.msgType.equals("turboAvailable")) {
                //send(new Turbo());
            	hasTurbo = true;
            }
            else if (msgFromServer.msgType.equals("crash")) {
                System.out.println("crashed");
                System.out.print("acceleration: " + acceleration + "\n");
                System.out.print("speed: " + speed + "\n");
            }
            else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("gameInit")) {
                System.out.println("Race init");
                System.out.println(msgFromServer.data);
                analyzeRoad(msgFromServer.data);
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
                System.out.println(msgFromServer.data);
                
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
            } else {
                send(new Ping());
            }
        }
    }
    
    class Piece
    {
    	LinkedTreeMap<Object, Object> piece;
    	
    	Piece(LinkedTreeMap<Object, Object> piece)
    	{
    		this.piece = piece;
    	}
    	
    	boolean isCurve()
    	{
    		return this.piece.containsKey("angle");
    	}
    	
    	double getAngle()
    	{
    		return (double) this.piece.get("angle");
    	}
    	
    	double getRadius()
    	{
    		return (double) this.piece.get("radius");
    	}
    	
    	double getLength()
    	{
    		if (this.isCurve())
    		{
    			return (double) Math.toRadians(getAngle()) * getRadius();		
    		}
    		else
    		{
    			return (double) this.piece.get("length");	
    		}
    	}
    }
    
    private void analyzeRoad(Object data)
    {
    	RoadMapParam roadMapParam = new RoadMapParam(data);
    	pieces = roadMapParam.getPieces();
    	
//    	System.out.print("size of pieces: " + pieces.size());
    	
    	for (int i=0; i<pieces.size(); i++)
    	{
    		LinkedTreeMap<Object, Object> piece = pieces.get(i);
    		
    		if (piece.containsKey("length"))
    		{
    			double length = (double) piece.get("length");
    			
//    			System.out.print("length: " + length + "\n");
    		}
    		else if (piece.containsKey("angle"))
    		{
    			double radius = (double) piece.get("radius");
    			double angle = (double) piece.get("angle");
    			
//    			System.out.print("radius: " + radius + ", angle: " + angle + "\n");
    			
    		}
    	}
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
    
    class RoadMapParam
    {
    	LinkedTreeMap<Object, Object> data;
    	LinkedTreeMap<Object, Object> race;
    	LinkedTreeMap<Object, Object> track;
    	ArrayList<LinkedTreeMap<Object, Object>> pieces;
    	
    	RoadMapParam(Object data)
    	{
    		this.data = (LinkedTreeMap<Object, Object>) data;
    		this.race = (LinkedTreeMap<Object, Object>) this.data.get("race");
    		this.track = (LinkedTreeMap<Object, Object>) this.race.get("track");
    		this.pieces = (ArrayList<LinkedTreeMap<Object, Object>>) this.track.get("pieces");
    	}
    	
    	ArrayList<LinkedTreeMap<Object, Object>> getPieces()
    	{
    		return this.pieces;
    	}
    }
    
    class CarPositionParam
    {
    	ArrayList<LinkedTreeMap<Object, Object>> data;
    	LinkedTreeMap<Object, Object> map;
    	LinkedTreeMap<Object, Object> piecePosition;
    	
    	CarPositionParam(Object data)
    	{
    		this.data = (ArrayList<LinkedTreeMap<Object, Object>>) data;
    		this.map = this.data.get(0);
    		this.piecePosition = (LinkedTreeMap<Object, Object>) this.map.get("piecePosition");
    	}
    	
    	double getAngle()
    	{
    		return (double) map.get("angle");
    	}
    	
    	double getPieceIndex()
    	{
    		return (double) piecePosition.get("pieceIndex");
    	}
    	
    	double getInPieceDistance()
    	{
    		return (double) piecePosition.get("inPieceDistance");
    	}
    	
    	double getLap()
    	{
    		return (double) piecePosition.get("lap");
    	}
    	
    }
}



abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class CreateRace extends SendMsg {
    public final String name;
    public final String key;
    public final String trackName;
    public final String password;
    public final int carCount;
    
    CreateRace(final String name, final String key, final String trackName,final String password, final int carCount) {
        this.name = name;
        this.key = key;
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }
    
    @Override
    protected Object msgData() {
        
    	JsonObject botId = new JsonObject();
    	botId.addProperty("name", this.name);
    	botId.addProperty("key", this.key);
    	
    	JsonObject data = new JsonObject();
    	data.addProperty("trackName", this.trackName);
    	data.addProperty("password", this.password);
    	data.addProperty("carCount", this.carCount);
    	data.add("botId", botId);
    	
    	return data;
    }

    @Override
    protected String msgType() {
        return "createRace";
    }
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Turbo extends SendMsg {
    @Override
	protected Object msgData() {
		// TODO Auto-generated method stub
		return "go go turbo";
	}

	@Override
    protected String msgType() {
        return "turbo";
    }
    
    
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}
